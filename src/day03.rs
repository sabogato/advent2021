
pub fn count_columns<'a, T>(readings: T) -> Vec<(u64, u64)>
    where T: Iterator<Item=&'a str>
{
    let mut columns: Vec<(u64, u64)> = vec![];

    for reading in readings {
        for (i, column) in reading.chars().enumerate() {
            if columns.len() < i+1 {
                columns.push((0,0));
            }
            match column {
                '0' => columns[i] = (columns[i].0 + 1, columns[i].1),
                '1' => columns[i] = (columns[i].0, columns[i].1 + 1),
                _ => (),
            }
        }
    }
    columns
}

pub fn calc_gamma_epsilon(columns: &[(u64, u64)]) -> (u64,u64)
{
    let mut gamma: u64 = 0;
    let mut epsilon: u64 = 0;

    for i in 0..columns.len() {
        let col = columns.len() - 1 - i;
        let bit: u64 = (1<<i).try_into().unwrap();

        let (zero_counts, one_counts) = columns[col];
        if one_counts > zero_counts {
            gamma += bit;
        }

        if one_counts < zero_counts {
            epsilon += bit;
        }
    }

    (gamma, epsilon)
}

pub fn part_1(columns: &[(u64, u64)]) -> u64 {
    let (g, e) = calc_gamma_epsilon(columns);
    g * e
}



#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE_READINGS: &str = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";
    const INPUT: &str = include_str!("../assets/day03-input.txt");

    #[test]
    fn test_part_1() {
        let counts = count_columns(EXAMPLE_READINGS.split("\n"));

        assert_eq!(
            part_1(&counts),
            198
        )
    }

    #[test]
    fn test_solution_1() {
        let counts = count_columns(INPUT.split("\n"));
        assert_eq!(part_1(&counts), 3633500);
    }
}
