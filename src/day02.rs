
use nom::combinator::map;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::sequence::preceded;
use nom::character::complete::i64;
use nom::multi::separated_list1;
use nom::character::complete::newline;
use nom::IResult;

#[derive(Debug, PartialEq)]
pub enum Command {
    Forward(i64),
    Down(i64),
    Up(i64),
}

pub fn parse_command(input: &str) -> IResult<&str, Command> {
    alt((
        map(preceded(tag("forward "), i64), Command::Forward),
        map(preceded(tag("down "), i64), Command::Down),
        map(preceded(tag("up "), i64), Command::Up),
    ))(input)
}

pub fn parse_commands(input: &str) -> IResult<&str, Vec<Command>> {
    separated_list1(newline, parse_command)(input)
}

#[derive(Debug, Default, PartialEq)]
pub struct Sub {
    pub depth: i64,
    pub horizontal: i64,
    pub aim: i64,
}

impl Sub {

    /// A version of on_command1 that mutates the Sub
    pub fn on_command1(&mut self, cmd: &Command) {
        match cmd {
            Command::Forward(x) => {
                self.horizontal += x;
            },
            Command::Up(x) => {
                self.depth -= x;
            }
            Command::Down(x) => {
                self.depth += x;
            }
        }
    }

    /// A version of on_command2 that mutates the Sub
    pub fn on_command2(&mut self, cmd: &Command) {
        match cmd {
            Command::Down(x) => {
                self.aim += x;
            }
            Command::Up(x) => {
                self.aim -= x;
            }
            Command::Forward(x) => {
                self.horizontal += x;
                self.depth = self.depth + self.aim * x;
            },
        }
    }
}

/// The part one fold function
pub fn on_command1(sub: Sub, cmd: &Command) -> Sub {
    match cmd {
        Command::Forward(x) => Sub { horizontal: sub.horizontal + x, ..sub },
        Command::Up(x) => Sub { depth: sub.depth - x, ..sub },
        Command::Down(x) => Sub { depth: sub.depth + x, ..sub },
    }
}

/// The part two fold function
pub fn on_command2(sub: Sub, cmd: &Command) -> Sub {
    match cmd {
        Command::Down(x) => Sub { aim: sub.aim + x, ..sub },
        Command::Up(x) => Sub { aim: sub.aim - x, ..sub },
        Command::Forward(x) => Sub {
            horizontal: sub.horizontal + x,
            depth: sub.depth + sub.aim * x,
            ..sub
        },
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const COMMANDS: &str = include_str!("../assets/day02-input.txt");

   #[test]
    fn test_example1() {
        let (_, commands) = parse_commands("forward 5
down 5
forward 8
up 3
down 8
forward 2").unwrap();

        assert_eq!(commands.iter().fold(Sub::default(), on_command1), Sub{ horizontal: 15, depth: 10, aim: 0});
    }

   #[test]
    fn test_example2() {
        let (_, commands) = parse_commands("forward 5
down 5
forward 8
up 3
down 8
forward 2").unwrap();

        assert_eq!(
            commands.iter().fold(
                Sub::default(), on_command2
            ),
            Sub{ horizontal: 15, depth: 60, aim: 10}
        );

    }

    #[test]
    fn test_day01_part1_functional() {
        let (_, commands) = parse_commands(COMMANDS).unwrap();

        // Test functional style:
        let sub = commands.iter().fold(Sub::default(), on_command1);

        assert_eq!(
            sub.depth * sub.horizontal,
            1660158,
        );
    }

    #[test]
    fn test_day01_part1_imperative() {
        let (_, commands) = parse_commands(COMMANDS).unwrap();

        // Test imperitive style:
        let mut sub = Sub::default();
        for cmd in commands.iter() {
            sub.on_command1(cmd);
        }
        assert_eq!(
            sub.depth * sub.horizontal,
            1660158,
        )
    }

    #[test]
    fn test_day01_part2_functional() {
        let (_, commands) = parse_commands(COMMANDS).unwrap();

        let sub = commands.iter().fold(Sub::default(), on_command2);

        assert_eq!(
            sub.depth * sub.horizontal,
            1604592846,
        );

        // Test imperitive style:
        let mut sub = Sub::default();
        for cmd in commands.iter() {
            sub.on_command2(cmd);
        }
        assert_eq!(
            sub.depth * sub.horizontal,
            1604592846,
        )
    }

    #[test]
    fn test_day01_imperative() {
        let (_, commands) = parse_commands(COMMANDS).unwrap();

        let sub = commands.iter().fold(Sub::default(), on_command2);

        assert_eq!(
            sub.depth * sub.horizontal,
            1604592846,
        );

        // Test imperitive style:
        let mut sub = Sub::default();
        for cmd in commands.iter() {
            sub.on_command2(cmd);
        }
        assert_eq!(
            sub.depth * sub.horizontal,
            1604592846,
        )
    }

    #[test]
    fn test_parse_command() {
        assert_eq!(
            parse_command("forward 10").unwrap(),
            ("", Command::Forward(10))
        );

        assert_eq!(
            parse_command("up 10").unwrap(),
            ("", Command::Up(10))
        );

        assert_eq!(
            parse_command("down 10").unwrap(),
            ("", Command::Down(10))
        );
    }
}
