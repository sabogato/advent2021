pub fn count_fishes(fishes: &[u64], days: u64) -> u64 {
    // we need a rolling window that is one more than the days it takes a fish
    // to give birth so we can count the new fish one index less than our
    // current day.
    let size = 9;
    let mut counts: [u64; 9] = [0,0,0,0,0,0,0,0,0];

    // count up the current fish and put those counts in the index of the days left
    for fish in fishes.iter() {
        let days_left = (fish + 1) % 7; // map the internal timer to the number of days left
        counts[days_left as usize] += 1;
    }

    for day in 0..days+1 {
        let idx = (day as usize) % size;
        let spawn = (idx + 7) as usize % size; // figure out the index 7 days into the future in our window
        counts[spawn] += counts[idx]; // add the spawned count to the next day in the window
    }

    counts.iter().sum()
}





#[cfg(test)]
mod tests {
    use nom::character::complete::newline;
    use nom::IResult;
    use nom::sequence::terminated;
    use nom::multi::separated_list1;
    use nom::bytes::complete::tag;
    use nom::character::complete::u64;
    use super::*;

    pub fn parse_input(input: &str) -> IResult<&str, Vec<u64>> {
        terminated(separated_list1(tag(","), u64), newline)(input)
    }

    const EXAMPLE: &str = "3,4,3,1,2\n";
    const INPUT: &str = include_str!("../assets/day06-input.txt");

    #[test]
    fn test_example() {
        let (_, example) = parse_input(EXAMPLE.clone()).unwrap();

        assert_eq!(
            count_fishes(&example, 18),
            26,
        )
    }

    #[test]
    fn test_part1() {
        let (_, input) = parse_input(INPUT).unwrap();

        assert_eq!(
            count_fishes(&input, 80),
            390011,
        );
    }

    #[test]
    fn test_part2() {
        let (_, fishes) = parse_input(INPUT).unwrap();
        let count = count_fishes(&fishes, 256);
        assert_eq!(
            count,
            1746710169834,
        );

    }
}
