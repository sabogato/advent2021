/// return the number of depths that increased from the previous one
pub fn depth_increase(depths: &[u32]) -> usize {
    if depths.len() < 2 {
        return 0;
    }
    let pairs = depths[..].iter().zip(depths[1..].iter());
    count_increased(pairs)
}

/// takes a window of 3 numbers and returns the number of windows that increased
/// from the previous one
pub fn depth_increase_windowed(depths: &[u32]) -> usize {
    // need at least 4 numbers in order to have 2 windows
    if depths.len() < 4 {
        return 0;
    }
    let sums = depths.windows(3).map(|w| -> u32 { w.iter().sum() });
    let sums_tail = sums.clone().skip(1);
    let pairs = sums.zip(sums_tail);
    count_increased(pairs)
}

/// counts if a pair increased
fn count_increased<I, X>(pairs: I) -> usize
where
    I: Iterator<Item = (X, X)>,
    X: PartialOrd,
{
    pairs.filter(|(x, y)| y > x).count()
}

#[cfg(test)]
mod tests {
    use crate::day01::*;
    use nom::character::complete::newline;
    use nom::character::complete::u32;
    use nom::multi::separated_list1;
    use nom::IResult;

    const DEPTHS: &str = include_str!("../assets/day01-input.txt");

    fn parse_depths(input: &str) -> IResult<&str, Vec<u32>> {
        separated_list1(newline, u32)(input)
    }

    #[test]
    fn test_depth_increase() {
        let (_input, depths) = parse_depths(DEPTHS).unwrap();
        println!("{:?}", depths);
        let result = depth_increase(&depths[..]);
        assert_eq!(result, 1451);
    }

    #[test]
    fn test_depth_increase_windowed() {
        let (_input, depths) = parse_depths(DEPTHS).unwrap();
        println!("{:?}", depths);
        let result = depth_increase_windowed(&depths[..]);
        assert_eq!(result, 1395);
    }
}
