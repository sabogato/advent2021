use advent2021::day02::parse_commands;
use advent2021::day02::Sub;
use advent2021::day02::on_command1;
use criterion::{criterion_group, criterion_main, Criterion};


const SUB_COMMANDS: &str = include_str!("../assets/day02-input.txt");

fn criterion_benchmark(c: &mut Criterion) {
    let (_, commands) = parse_commands(SUB_COMMANDS).unwrap();

    let mut group = c.benchmark_group("day02.1");

    group.bench_function("functional", |b| b.iter(|| {
        commands.iter().fold(Sub::default(), on_command1);
    }));

    group.bench_function("imperative", |b| b.iter(|| {
        let mut sub = Sub::default();
        for cmd in commands.iter() {
            sub.on_command1(cmd);
        }
    }));
    group.finish();
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
